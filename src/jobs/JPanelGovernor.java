/*
 * Copyright (C) 2016 niteip
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jobs;

/**
 *
 * @author niteip
 */
public abstract class JPanelGovernor extends javax.swing.JPanel implements Runnable {
    private Thread t;
    private final String tN;

    public JPanelGovernor(String tN) {
        this.tN = tN;
    }

    protected abstract void getData();
    
    @Override
    public void run() {
        try {
            System.out.println(tN);
            getData();
            Thread.sleep(50);
        } catch (InterruptedException ex) {
            //Logger.getLogger(JPanel_Transkript.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void start ()
    {
        System.out.println("Thread " +  tN + " starting!");
        if (t == null) {
           t = new Thread (this, tN);
           t.start ();
        }
    }
    
}
