/*
 * Copyright (C) 2015 Çağrı ULAŞ <cagriulas [@] gmail [.] com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jobs;

/**
 *
 * @author Çağrı ULAŞ <cagriulas [@] gmail [.] com>
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.event.EventListenerList;

/**
 *
 * @author Çağrı ULAŞ <cagriulas [@] gmail [.] com>
 */
public class SpecialButtonGroup extends ButtonGroup {
    private final EventListenerList buttonListenerList = new EventListenerList();
    private final ActionListener buttonGroupListener = new BtnGrpListener();

    @Override
    public void add(AbstractButton b) {
       b.addActionListener(buttonGroupListener);
       super.add(b);
    }

    public void addActionListener(ActionListener listener) {
       buttonListenerList.add(ActionListener.class, listener);
    }

    protected void fireActionListeners() {
       Object[] listeners = buttonListenerList.getListenerList();
       String actionCommand = "";
       ButtonModel model = getSelection();
       if (model != null) {
          actionCommand = model.getActionCommand();
       }
       ActionEvent ae = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, actionCommand);
       for (int i = 0; i < listeners.length; ++i) {
           if (listeners[i] == ActionListener.class)
               ((ActionListener)listeners[i+1]).actionPerformed(ae);
       }
    }

    private class BtnGrpListener implements ActionListener {

       @Override
       public void actionPerformed(ActionEvent ae) {
          fireActionListeners();
       }
    }
}
