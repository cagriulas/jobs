/*
 * Copyright (C) 2015 Çağrı ULAŞ <cagriulas [@] gmail [.] com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jobs;

import Handlers.HDBHandler;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import jobs.EntityClasses.Student;
import jobs.EntityClasses.Teacher;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Çağrı ULAŞ <cagriulas [@] gmail [.] com>
 */
public class MainJFrame extends javax.swing.JFrame {

    /**
     * Creates new form MainJFrame
     */
    private SpecialButtonGroup group;
    private String[] buttonNames;
    private JToggleButton tbtn = null;
    private String userID;
    
    public MainJFrame(String token, String userID) {
        initComponents();
        
        this.userID=userID;
        group = new SpecialButtonGroup();
        
        if ("s".equals(token)) {
            initStudent(userID);
        } else if ("t".equals(token)) {
            initTeacher(userID);
        }
        
        initButtonListener();
    }
    
    private void initStudent(String userID) {
        Transaction tx = null;
        HDBHandler h = new HDBHandler();
        
        Session s = h.openSession();
        tx = s.beginTransaction();
       
        String hql = "FROM Student as t where t.studentno = :userID";
        Query query = s.createQuery(hql);
        query.setString("userID",userID);
        List l = query.list();
        
        for (Iterator iterator = l.iterator(); iterator.hasNext();){
            Student t = (Student) iterator.next(); 
            jLabel_Name.setText("Öğrenci: "+t.getFirstname()+" "+t.getLastname());
            jLabel_Term.setText("Dönem: "+t.getSemester());
        }
        try{
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        h.closeSession();
        
                
        buttonNames = new String[]{"Bilgiler","Not Durum Belgesi","Ders Seçme","Danışman Bilgisi"};
        buttonCreator(buttonNames);
    }

    private void initTeacher(String userID) {
        
        Transaction tx = null;
        HDBHandler h = new HDBHandler();
        
        Session s = h.openSession();
        tx = s.beginTransaction();
       
        String hql = "FROM Teacher as t where t.tcno = :userID";
        Query query = s.createQuery(hql);
        query.setString("userID",userID);
        List l = query.list();
        
        for (Iterator iterator = l.iterator(); iterator.hasNext();){
            Teacher t = (Teacher) iterator.next(); 
            jLabel_Name.setText("Öğretmen: "+t.getFirstname()+" "+t.getLastname());
        }
        try{
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        h.closeSession();
        
        buttonNames = new String[]{"Bilgiler","Dersler","Sınav Sonucu Gir","Ders Onayla","Danışman Öğrencileri"};
        buttonCreator(buttonNames);
        
        
    }
    
    private void buttonCreator (String[] buttonNames) {
        int vloc = 0;
        for (String b : buttonNames) {
            tbtn = new JToggleButton(b);
            tbtn.setActionCommand(b);
            group.add(tbtn);
            //if (vloc == 0) tbtn.setSelected(true);
            tbtn.setSize(200,30);
            tbtn.setLocation(0, vloc);
            vloc+=30;
            jPanel_Buttons.add(tbtn);
        }
    }
    
    private void initButtonListener() {
        group.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                String actionCommand = e.getActionCommand();
                
                jPanel_Infos.removeAll();
                int halfheight = jPanel_Infos.getHeight() / 2;
                int halfwidth = jPanel_Infos.getWidth() / 2;
                
                JPanel p = null;
                
                if (buttonNames.length == 4) {
                    switch (actionCommand) {
                        case "Bilgiler":
                            p = new JPanel_StudentInfo(userID);
                            break;
                        case "Not Durum Belgesi":
                            p = new JPanel_Transkript(userID);
                            break;
                        case "Ders Seçme":
                            p = new JPanel_SelectCourses(userID);
                            break;
                        case "Danışman Bilgisi":
                            p = new JPanel_Consultant(userID);
                            break;
                        default:
                            break;
                    }
                }
                else {
                    switch (actionCommand) {
                        case "Bilgiler":
                            p = new JPanel_TeacherInfo(userID);
                            break;
                        case "Dersler":
                            p = new JPanel_TeacherCourses(userID);
                            break;
                        case "Sınav Sonucu Gir":
                            p = new JPanel_ExamResult(userID);
                            break;
                        case "Ders Onayla":
                            p = new JPanel_CourseValidation(userID);
                            break;
                        case "Danışman Öğrencileri":
                            p = new JPanel_ConsultantStudents(userID);
                            break;
                        default:
                            break;
                    }
                }
                panelLocater(p, halfwidth, halfheight);
                jPanel_Infos.add(p);
                if (p instanceof JPanelGovernor) {
                    ((JPanelGovernor) p).start();
                }
                jPanel_Infos.updateUI();
                jPanel_Infos.revalidate();
            }
        });
    }
    
    private void panelLocater(JPanel panel, int halfwidth, int halfheight) {
        Dimension d = panel.getPreferredSize();
        panel.setSize(d);
        //panel.setSize(pWidth, pHeight);
        panel.setLocation(halfwidth-(int)d.getWidth()/2, halfheight-(int)d.getHeight()/2);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel_Name = new javax.swing.JLabel();
        jLabel_Term = new javax.swing.JLabel();
        jPanel_Buttons = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel_Infos = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel_Buttons.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel_Buttons.setMaximumSize(new java.awt.Dimension(200, 32767));
        jPanel_Buttons.setPreferredSize(new java.awt.Dimension(200, 0));

        javax.swing.GroupLayout jPanel_ButtonsLayout = new javax.swing.GroupLayout(jPanel_Buttons);
        jPanel_Buttons.setLayout(jPanel_ButtonsLayout);
        jPanel_ButtonsLayout.setHorizontalGroup(
            jPanel_ButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 198, Short.MAX_VALUE)
        );
        jPanel_ButtonsLayout.setVerticalGroup(
            jPanel_ButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 458, Short.MAX_VALUE)
        );

        jPanel_Infos.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel_Infos.setAutoscrolls(true);
        jPanel_Infos.setPreferredSize(new java.awt.Dimension(700, 470));

        javax.swing.GroupLayout jPanel_InfosLayout = new javax.swing.GroupLayout(jPanel_Infos);
        jPanel_Infos.setLayout(jPanel_InfosLayout);
        jPanel_InfosLayout.setHorizontalGroup(
            jPanel_InfosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 680, Short.MAX_VALUE)
        );
        jPanel_InfosLayout.setVerticalGroup(
            jPanel_InfosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jLabel1.setFont(new java.awt.Font("Droid Sans", 1, 18)); // NOI18N
        jLabel1.setText("Öğrenci Bilgi Sistemi");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator1)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel_Name, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel_Term, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jPanel_Buttons, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel_Infos, javax.swing.GroupLayout.DEFAULT_SIZE, 682, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel_Name)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel_Term))
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel_Infos, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
                    .addComponent(jPanel_Buttons, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new MainJFrame().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel_Name;
    private javax.swing.JLabel jLabel_Term;
    private javax.swing.JPanel jPanel_Buttons;
    private javax.swing.JPanel jPanel_Infos;
    private javax.swing.JSeparator jSeparator1;
    // End of variables declaration//GEN-END:variables
}
