/*
 * Copyright (C) 2015 Çağrı ULAŞ <cagriulas [@] gmail [.] com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jobs;

/**
 *
 * @author Çağrı ULAŞ <cagriulas [@] gmail [.] com>
 */
import Handlers.HDBHandler;
import java.awt.Component;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.swing.JToggleButton;
import javax.swing.table.DefaultTableModel;
import jobs.EntityClasses.Course;
import jobs.EntityClasses.CourseNResult;
import jobs.EntityClasses.Student;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author niteip
 */
public class JPanel_SelectCourses extends JPanelGovernor {
    private HashMap<String, Course> courseCodes;
    private String userID;
    private List l;
    
    /**
     * Creates new form JPanel_SelectCourses
     * @param userID
     */
    public JPanel_SelectCourses(String userID) {
        super(userID);
        initComponents();
        this.userID=userID;
//        initToggleButtons();
//        tuneToggleButtons();
    }

    @Override
    protected final void getData() {
        initToggleButtons();
        tuneToggleButtons();
    }
    
    
    
    private void initToggleButtons() {
        
        HDBHandler h = new HDBHandler();
        Session s = h.openSession();
        Transaction tx = s.beginTransaction();
        
        courseCodes = new HashMap<>();
        
        //fetch courses related for this students semester
        DetachedCriteria subCriteria = DetachedCriteria.forClass(Student.class);
        subCriteria.add(Property.forName("studentno").eq(userID));
        subCriteria.setProjection(Projections.property("semester"));
        
        DetachedCriteria criteria = DetachedCriteria.forClass(Course.class);
        criteria.add(Property.forName("semester").le(subCriteria));
        Criteria c = criteria.getExecutableCriteria(s);
        
        l=c.list();
        
        for (Iterator iterator = l.iterator(); iterator.hasNext();){
            Course t = (Course) iterator.next(); 
            courseCodes.put(t.getCode(), t);
        }
        
        try{
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        h.closeSession();
  
        int vloc = 0;
        for (Object b : l) {
            JToggleButton tbtn = new JToggleButton(((Course)b).getCoursename());
            tbtn.setName(((Course)b).getCode());
            tbtn.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    tbtnActionPerformed(evt);
                }
            });
            tbtn.setActionCommand(((Course)b).getCode());
            tbtn.setSize(200,30);
            tbtn.setLocation(0, vloc);
            vloc+=30;
            jPanel_Courses.add(tbtn);
        }
    }
    
    private void tuneToggleButtons() {
        HDBHandler h = new HDBHandler();
        Session s = h.openSession();
        Transaction tx = s.beginTransaction();
        
        Criteria crit = s.createCriteria(CourseNResult.class);
        crit.setProjection(Projections.property("code"));
        crit.add(Restrictions.sqlRestriction("visa*0.40+final*0.60 < 50"));
        crit.add(Restrictions.eq("studentno.studentno", userID));
        List l = crit.list();
        
        for (Object o : l) {
            Course crs = (Course)o ;
            if (courseCodes.containsValue(crs) && !crs.getOptional()) {
                String code = crs.getCode();
                Component[] c = jPanel_Courses.getComponents();

                for (Object b : c) {
                    JToggleButton btn = ((JToggleButton)b);
                    if (code.equals( btn.getName() )) {
                        btn.doClick();
                        btn.setEnabled(false);
                    }
                }
            }
        }
        
        try{
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        h.closeSession();
        
        jPanel_Courses.updateUI();
        jPanel_Courses.revalidate();
    }

    private void tbtnActionPerformed(java.awt.event.ActionEvent evt) {                                               
        // TODO add your handling code here:
        DefaultTableModel model = (DefaultTableModel) jTable.getModel();
        
        for (int i = model.getRowCount()-1; i >= 0; i--)
            model.removeRow(i);
        
        int i = 0;
        int totalCredit = 0;
        for (Iterator iterator = l.iterator(); iterator.hasNext();){
            Course t = (Course) iterator.next();
            Component[] c = jPanel_Courses.getComponents();
            JToggleButton b = (JToggleButton) c[i];
            if (b.isSelected() == true) {
                totalCredit += t.getCredit();
                model.addRow(new Object[]{t.getCoursename(), t.getCode(), t.getCredit()});
            }
            i++;
        }
        
        jLabel_AvailableCredit.setText( String.valueOf(totalCredit) +"/36");
    }     
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel_AvailableCredit = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel_Courses = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        jButton_Finish = new javax.swing.JButton();

        jLabel_AvailableCredit.setText("0/36");

        jLabel4.setText("Kredi:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addGap(31, 31, 31)
                .addComponent(jLabel_AvailableCredit)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel_AvailableCredit))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel_CoursesLayout = new javax.swing.GroupLayout(jPanel_Courses);
        jPanel_Courses.setLayout(jPanel_CoursesLayout);
        jPanel_CoursesLayout.setHorizontalGroup(
            jPanel_CoursesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 200, Short.MAX_VALUE)
        );
        jPanel_CoursesLayout.setVerticalGroup(
            jPanel_CoursesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Ders Adı", "Ders Kodu", "Kredi"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable);

        jButton_Finish.setText("Tamamla");
        jButton_Finish.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_FinishActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel_Courses, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 434, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton_Finish)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton_Finish, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 309, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel_Courses, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_FinishActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_FinishActionPerformed
        // TODO add your handling code here:
        
        HDBHandler h = new HDBHandler();
        Session s = h.openSession();
        Transaction tx;
        
        Component[] c = jPanel_Courses.getComponents();
        for (Object b : c) {
            JToggleButton btn = ((JToggleButton)b);
            if ( btn.isSelected() ) {
                Criteria crit = s.createCriteria(CourseNResult.class);
                crit.add(Restrictions.eq("studentno.studentno", userID));
                crit.add(Restrictions.eq("code.code",btn.getName()));
                List l = crit.list();
                tx = s.beginTransaction();
                if (l.isEmpty()){
                    CourseNResult cnr = new CourseNResult();
                    Student st = new Student(userID);

                    cnr.setCode(courseCodes.get(btn.getName()));
                    cnr.setStudentno(st);
                    cnr.setFinal1(-1);
                    cnr.setVisa(-1);
                    cnr.setValid(Boolean.FALSE);
                    s.save(cnr);
                   
                }
                else{
                    for (Object o : l) {
                        CourseNResult cs = (CourseNResult) o;
                        cs.setFinal1(-1);
                        cs.setVisa(-1);
                        cs.setValid(Boolean.FALSE);
                        s.update(cs);
                    }      
                }
              
                try{
                    if(!tx.wasCommitted())
                        tx.commit();
                } 
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        h.closeSession();
    }//GEN-LAST:event_jButton_FinishActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton_Finish;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel_AvailableCredit;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel_Courses;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable;
    // End of variables declaration//GEN-END:variables
}
