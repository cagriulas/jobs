/*
 * Copyright (C) 2015 Çağrı ULAŞ <cagriulas [@] gmail [.] com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jobs.EntityClasses;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Çağrı ULAŞ <cagriulas [@] gmail [.] com>
 */
@Entity
@Table(name = "Student", catalog = "postgres", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Student.findAll", query = "SELECT s FROM Student s"),
    @NamedQuery(name = "Student.findByLastname", query = "SELECT s FROM Student s WHERE s.lastname = :lastname"),
    @NamedQuery(name = "Student.findByFirstname", query = "SELECT s FROM Student s WHERE s.firstname = :firstname"),
    @NamedQuery(name = "Student.findByTcno", query = "SELECT s FROM Student s WHERE s.tcno = :tcno"),
    @NamedQuery(name = "Student.findByStudentno", query = "SELECT s FROM Student s WHERE s.studentno = :studentno"),
    @NamedQuery(name = "Student.findByBirthdate", query = "SELECT s FROM Student s WHERE s.birthdate = :birthdate"),
    @NamedQuery(name = "Student.findByPnumber", query = "SELECT s FROM Student s WHERE s.pnumber = :pnumber"),
    @NamedQuery(name = "Student.findByEmail", query = "SELECT s FROM Student s WHERE s.email = :email"),
    @NamedQuery(name = "Student.findByAdress", query = "SELECT s FROM Student s WHERE s.adress = :adress"),
    @NamedQuery(name = "Student.findBySemester", query = "SELECT s FROM Student s WHERE s.semester = :semester")})
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "lastname", length = 20)
    private String lastname;
    @Column(name = "firstname", length = 20)
    private String firstname;
    @Column(name = "tcno", length = 11)
    private String tcno;
    @Id
    @Basic(optional = false)
    @Column(name = "studentno", nullable = false, length = 9)
    private String studentno;
    @Column(name = "birthdate", length = 20)
    private String birthdate;
    @Column(name = "pnumber", length = 20)
    private String pnumber;
    @Column(name = "email", length = 20)
    private String email;
    @Column(name = "adress", length = 40)
    private String adress;
    @Column(name = "semester", length = 2147483647)
    private String semester;
    @JoinColumn(name = "consultantno", referencedColumnName = "tcno")
    @ManyToOne
    private Teacher consultantno;
    @OneToMany(mappedBy = "studentno")
    private Collection<CourseNResult> courseNResultCollection;

    public Student() {
    }

    public Student(String studentno) {
        this.studentno = studentno;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getTcno() {
        return tcno;
    }

    public void setTcno(String tcno) {
        this.tcno = tcno;
    }

    public String getStudentno() {
        return studentno;
    }

    public void setStudentno(String studentno) {
        this.studentno = studentno;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getPnumber() {
        return pnumber;
    }

    public void setPnumber(String pnumber) {
        this.pnumber = pnumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public Teacher getConsultantno() {
        return consultantno;
    }

    public void setConsultantno(Teacher consultantno) {
        this.consultantno = consultantno;
    }

    @XmlTransient
    public Collection<CourseNResult> getCourseNResultCollection() {
        return courseNResultCollection;
    }

    public void setCourseNResultCollection(Collection<CourseNResult> courseNResultCollection) {
        this.courseNResultCollection = courseNResultCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (studentno != null ? studentno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Student)) {
            return false;
        }
        Student other = (Student) object;
        if ((this.studentno == null && other.studentno != null) || (this.studentno != null && !this.studentno.equals(other.studentno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jobs.EntityClasses.Student[ studentno=" + studentno + " ]";
    }
    
}
