/*
 * Copyright (C) 2015 Çağrı ULAŞ <cagriulas [@] gmail [.] com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jobs.EntityClasses;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Çağrı ULAŞ <cagriulas [@] gmail [.] com>
 */
@Entity
@Table(name = "Course", catalog = "postgres", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Course.findAll", query = "SELECT c FROM Course c"),
    @NamedQuery(name = "Course.findByCoursename", query = "SELECT c FROM Course c WHERE c.coursename = :coursename"),
    @NamedQuery(name = "Course.findByCode", query = "SELECT c FROM Course c WHERE c.code = :code"),
    @NamedQuery(name = "Course.findByCredit", query = "SELECT c FROM Course c WHERE c.credit = :credit"),
    @NamedQuery(name = "Course.findBySemester", query = "SELECT c FROM Course c WHERE c.semester = :semester"),
    @NamedQuery(name = "Course.findByOptional", query = "SELECT c FROM Course c WHERE c.optional = :optional")})
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "coursename", length = 20)
    private String coursename;
    @Id
    @Basic(optional = false)
    @Column(name = "code", nullable = false, length = 10)
    private String code;
    @Column(name = "credit")
    private Integer credit;
    @Column(name = "semester", length = 10)
    private String semester;
    @Column(name = "optional")
    private Boolean optional;
    @JoinColumn(name = "consultantno", referencedColumnName = "tcno")
    @ManyToOne
    private Teacher consultantno;
    @OneToMany(mappedBy = "code")
    private Collection<CourseNResult> courseNResultCollection;

    public Course() {
    }

    public Course(String code) {
        this.code = code;
    }

    public String getCoursename() {
        return coursename;
    }

    public void setCoursename(String coursename) {
        this.coursename = coursename;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getCredit() {
        return credit;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public Boolean getOptional() {
        return optional;
    }

    public void setOptional(Boolean optional) {
        this.optional = optional;
    }

    public Teacher getConsultantno() {
        return consultantno;
    }

    public void setConsultantno(Teacher consultantno) {
        this.consultantno = consultantno;
    }

    @XmlTransient
    public Collection<CourseNResult> getCourseNResultCollection() {
        return courseNResultCollection;
    }

    public void setCourseNResultCollection(Collection<CourseNResult> courseNResultCollection) {
        this.courseNResultCollection = courseNResultCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Course)) {
            return false;
        }
        Course other = (Course) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jobs.EntityClasses.Course[ code=" + code + " ]";
    }
    
}
