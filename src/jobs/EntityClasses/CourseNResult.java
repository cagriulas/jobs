/*
 * Copyright (C) 2015 Çağrı ULAŞ <cagriulas [@] gmail [.] com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jobs.EntityClasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Çağrı ULAŞ <cagriulas [@] gmail [.] com>
 */
@Entity
@Table(name = "CourseNResult", catalog = "postgres", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CourseNResult.findAll", query = "SELECT c FROM CourseNResult c"),
    @NamedQuery(name = "CourseNResult.findById", query = "SELECT c FROM CourseNResult c WHERE c.id = :id"),
    @NamedQuery(name = "CourseNResult.findByVisa", query = "SELECT c FROM CourseNResult c WHERE c.visa = :visa"),
    @NamedQuery(name = "CourseNResult.findByFinal1", query = "SELECT c FROM CourseNResult c WHERE c.final1 = :final1"),
    @NamedQuery(name = "CourseNResult.findByValid", query = "SELECT c FROM CourseNResult c WHERE c.valid = :valid")})
public class CourseNResult implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "visa")
    private Integer visa;
    @Column(name = "final")
    private Integer final1;
    @Column(name = "valid")
    private Boolean valid;
    @JoinColumn(name = "studentno", referencedColumnName = "studentno")
    @ManyToOne
    private Student studentno;
    @JoinColumn(name = "code", referencedColumnName = "code")
    @ManyToOne
    private Course code;

    public CourseNResult() {
    }

    public CourseNResult(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVisa() {
        return visa;
    }

    public void setVisa(Integer visa) {
        this.visa = visa;
    }

    public Integer getFinal1() {
        return final1;
    }

    public void setFinal1(Integer final1) {
        this.final1 = final1;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public Student getStudentno() {
        return studentno;
    }

    public void setStudentno(Student studentno) {
        this.studentno = studentno;
    }

    public Course getCode() {
        return code;
    }

    public void setCode(Course code) {
        this.code = code;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CourseNResult)) {
            return false;
        }
        CourseNResult other = (CourseNResult) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jobs.EntityClasses.CourseNResult[ id=" + id + " ]";
    }
    
}
