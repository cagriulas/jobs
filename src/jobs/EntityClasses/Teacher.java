/*
 * Copyright (C) 2015 Çağrı ULAŞ <cagriulas [@] gmail [.] com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jobs.EntityClasses;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Çağrı ULAŞ <cagriulas [@] gmail [.] com>
 */
@Entity
@Table(name = "Teacher", catalog = "postgres", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Teacher.findAll", query = "SELECT t FROM Teacher t"),
    @NamedQuery(name = "Teacher.findByFirstname", query = "SELECT t FROM Teacher t WHERE t.firstname = :firstname"),
    @NamedQuery(name = "Teacher.findByLastname", query = "SELECT t FROM Teacher t WHERE t.lastname = :lastname"),
    @NamedQuery(name = "Teacher.findByTcno", query = "SELECT t FROM Teacher t WHERE t.tcno = :tcno"),
    @NamedQuery(name = "Teacher.findByBirthdate", query = "SELECT t FROM Teacher t WHERE t.birthdate = :birthdate"),
    @NamedQuery(name = "Teacher.findByPnumber", query = "SELECT t FROM Teacher t WHERE t.pnumber = :pnumber"),
    @NamedQuery(name = "Teacher.findByEmail", query = "SELECT t FROM Teacher t WHERE t.email = :email"),
    @NamedQuery(name = "Teacher.findByAdress", query = "SELECT t FROM Teacher t WHERE t.adress = :adress")})
public class Teacher implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "firstname", length = 20)
    private String firstname;
    @Column(name = "lastname", length = 20)
    private String lastname;
    @Id
    @Basic(optional = false)
    @Column(name = "tcno", nullable = false, length = 11)
    private String tcno;
    @Column(name = "birthdate", length = 20)
    private String birthdate;
    @Column(name = "pnumber", length = 20)
    private String pnumber;
    @Column(name = "email", length = 20)
    private String email;
    @Column(name = "adress", length = 40)
    private String adress;
    @OneToMany(mappedBy = "consultantno")
    private Collection<Student> studentCollection;
    @OneToMany(mappedBy = "consultantno")
    private Collection<Course> courseCollection;

    public Teacher() {
    }

    public Teacher(String tcno) {
        this.tcno = tcno;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getTcno() {
        return tcno;
    }

    public void setTcno(String tcno) {
        this.tcno = tcno;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getPnumber() {
        return pnumber;
    }

    public void setPnumber(String pnumber) {
        this.pnumber = pnumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    @XmlTransient
    public Collection<Student> getStudentCollection() {
        return studentCollection;
    }

    public void setStudentCollection(Collection<Student> studentCollection) {
        this.studentCollection = studentCollection;
    }

    @XmlTransient
    public Collection<Course> getCourseCollection() {
        return courseCollection;
    }

    public void setCourseCollection(Collection<Course> courseCollection) {
        this.courseCollection = courseCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tcno != null ? tcno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Teacher)) {
            return false;
        }
        Teacher other = (Teacher) object;
        if ((this.tcno == null && other.tcno != null) || (this.tcno != null && !this.tcno.equals(other.tcno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jobs.EntityClasses.Teacher[ tcno=" + tcno + " ]";
    }
    
}
