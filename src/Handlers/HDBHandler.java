/*
 * Copyright (C) 2015 Çağrı ULAŞ <cagriulas [@] gmail [.] com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Handlers;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;


/**
 *
 * @author Çağrı ULAŞ <cagriulas [@] gmail [.] com>
 */

public class HDBHandler {
//    private static SessionFactory factory;
//    private static ServiceRegistry registry;
//    private static Session openedSession;
    
    private static Configuration configuration;
    private static StandardServiceRegistryBuilder builder;
    private static SessionFactory sessionFactory;
    private static Session session;

    public HDBHandler() {
        createFactory();
    }
    
    private void createFactory() {
        configuration = new Configuration().configure();
        builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        sessionFactory = configuration.buildSessionFactory(builder.build());
    }
    
    public Session openSession() {
        return sessionFactory.openSession();
    }
    
    public void closeSession() {
        if ( session != null )
            session.close();
    }
    
        
//    public static SessionFactory CreateSessionFactory() {
//        try{
//            Configuration config = new Configuration();
//            registry = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
//            factory = config.configure().buildSessionFactory(registry);
//        }catch (Throwable ex) { 
//            System.err.println("Failed to create sessionFactory object." + ex);
//            throw new ExceptionInInitializerError(ex); 
//        }
//        return factory;
//    }
//    
//    public static SessionFactory getSessionFactory() {
//
//        Configuration configuration = new Configuration().configure();
//        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
//        SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
//        return sessionFactory;
//
//    }
//    
//    public static Session OpenSession () {
//        openedSession = CreateSessionFactory().openSession();
//        return openedSession;
//    }
//    
//    public static void CloseSession () {
//        openedSession.close();
//    }
}
